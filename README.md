# Overview
Start with a clone of this project to create a Spring Boot Microservice bundled up into a Docker container.

Intention was to provide a solution to many of the moving parts in one place. Fed up having to go through the same steps, read the same material, just to get back here every time.

This is phase 1. The Microservice without any embellishments.

# Building
Assuming that Sonar is installed and configured and that Docker is installed on build machine:

	mvn clean package sonar:sonar
	mvn docker:build

__NOTE__: Sonar can be run locally from a Docker container requiring no additional configuration:

	docker run -d\
	 --name sonarqube\
	 -p 9000:9000\
	 -p 9092:9092\
	 sonarqube

	http://localhost:9000

# Running
General form of the run command is:

	docker run -d\
	 --env-file some-file.env\
	 -p 8080:8080\
	 ${}/${}

Where __some-file.env__ is a list of environment variables matching __application.properties__ to be added/overridden.

Out of the box the application can be run like this:

	docker run -d\
	 -p 8080:8080\
	 mark_meany/spring-boot-microservice-starter

or for some variation:

	docker run -d\
	 -e "APP_GREETING_TEMPLATE=Welcome to my world %s!"\
	 -p 8080:8080\
	 mark_meany/spring-boot-microservice-starter

And can be tested:

	curl -v -H "Accept: application/json" http://localhost:8080/greet/Mark

# Application Properties
From the start, use Spring profiles. To this end the application comes with __application-default.properties__ located in __/src/main/resources__ where properties that do not change can be put. This file will be baked into the JAR, do not put any sensitive information in here!

In __/src/test/resources__ can be found __application-test.properties__. Integration tests make use of this, the test classes are annotated with __@ActiveProfiles("test")__ that makes sure this file is loaded.

# Logging
Logback has been used and there is a baked in __logback.xml__ that sets root logging level to WARN. 

Finer grained logging could be done using property values in an environment specific manner. This has been done for tests, the following entry in ____ sees to that:

	logging.level.com.mvmlabs=DEBUG

# Testing
Various bits of testing in here that make use of:

* JUnit, Hamcrest and Mockito
* Spring Test Framework to test controller standalone with Mockito (__GreetingEndpointStandAloneWebMvcTests__)
* Spring Test Framework to test controller using JSONPath (__GreetingEndpointWebMvcTests__)
* Integration test using Spring Test Framework with RestAssured to test controller over the wire (__GreetingEndpointWebMvcIntegrationTests__)
* Spring Test Framework to test Services (__GreetingServiceSimpleImplIntegrationTests__)

TODO: Cucumber integration tests

# Code metrics
A normal build will produce a JaCoCo code coverage report.

Maven is loaded with Sonar plugin, so if Sonar is around that can be used to gather stats.

# Docker Packaging
Maven is used to perform the Docker build. This requires Docker to be installed on the build machine. The build is achieved thusly:

	mvnw clean install
	mvnw docker:build

The Dockerfile is located at __src/main/docker/Dockerfile__, note that it is parameterised and cannot be used as-is:

	FROM frolvlad/alpine-oraclejdk8:slim
	VOLUME /tmp
	ADD @project.build.finalName@.jar app.jar
	RUN sh -c 'touch /app.jar'
	ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

The Maven POM makes use of the Resources plugin to copy this file to a folder in the target directory - the copy process applies filtering that will ensure that the Dockerfile has been updated to identify the appropriately named jar file.

	<plugin>
		<artifactId>maven-resources-plugin</artifactId>
		<executions>
			<execution>
				<id>copy-resources</id>
				<!-- here the phase you need -->
				<phase>validate</phase>
				<goals>
					<goal>copy-resources</goal>
				</goals>
				<configuration>
					<outputDirectory>${basedir}/target/dockerfile</outputDirectory>
					<resources>
						<resource>
							<directory>src/main/docker</directory>
							<filtering>true</filtering>
						</resource>
					</resources>
				</configuration>
			</execution>
		</executions>
	</plugin>

That just leaves the Docker Build plugin:

	<plugin>
		<groupId>com.spotify</groupId>
		<artifactId>docker-maven-plugin</artifactId>
		<version>0.4.10</version>
		<configuration>
			<imageName>${docker-account-name}/${project.artifactId}</imageName>
			<dockerDirectory>${basedir}/target/dockerfile</dockerDirectory>
			<resources>
				<resource>
					<targetPath>/</targetPath>
					<directory>${project.build.directory}</directory>
					<include>${project.build.finalName}.jar</include>
				</resource>
			</resources>
		</configuration>
	</plugin>
 
This assumes no component repository for your build artifacts and also does not deploy to DockerHub or any other remote repository.

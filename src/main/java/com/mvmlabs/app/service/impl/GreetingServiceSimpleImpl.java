package com.mvmlabs.app.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mvmlabs.app.service.GreetingService;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GreetingServiceSimpleImpl implements GreetingService {
	
	@Getter
	@Value("${app.default.greeting.name:You}")
	private String defaultName;

	@Getter
	@Value("${app.greeting.template:Howdy %s!}")
	private String greetingTemplate;
	
	@Override
	public String createGreeting(String name) {
		log.debug("Generating a greeting for {} using template '{}'", name, greetingTemplate);
		return String.format(greetingTemplate, defaultNameWhenNullOrWhitespace(name));
	}

	private String defaultNameWhenNullOrWhitespace(final String name) {
		if (name == null || name.matches("\\s*")) {
			log.debug("Using default name as one provided was either null, empty or contained only whitespace.");
			return defaultName;
		}
		return name.trim();
	}
}

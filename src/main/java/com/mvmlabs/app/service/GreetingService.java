package com.mvmlabs.app.service;

@FunctionalInterface
public interface GreetingService {

	public String createGreeting(String name);
}

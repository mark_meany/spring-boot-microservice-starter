package com.mvmlabs.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTestingApplication {

	public SpringBootTestingApplication() {
		// Constructor required as this class is loaded as a Configuration class!
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootTestingApplication.class, args);
	}
}

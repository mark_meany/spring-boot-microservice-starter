package com.mvmlabs.app.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mvmlabs.app.service.GreetingService;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class GreetingEndpoint {

	@Autowired
	private GreetingService greetingService;
	
	@AllArgsConstructor
	private static class Greeting {
		@Getter
		private String message;
	}

	@RequestMapping(value = "/greet/{name}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<Greeting> greet(@PathVariable("name") final String name) {
		log.debug("Request received to generate a greeting for '{}'", name);
		return new ResponseEntity<>(new Greeting(greetingService.createGreeting(name)), HttpStatus.OK);
	}
	
}

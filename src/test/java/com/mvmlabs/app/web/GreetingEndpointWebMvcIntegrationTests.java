package com.mvmlabs.app.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.mvmlabs.app.SpringBootTestingApplication;
import com.mvmlabs.app.service.GreetingService;

import io.restassured.RestAssured;

import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Test controller in MVC environment using Spring test framework.
 * 
 * As it is annotated as an Integration Test the service will be started and Rest Assured used to fire requests at it over the wire.
 * 
 * @author mark
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = SpringBootTestingApplication.class)
@IntegrationTest("server.port:0") // Use a random port
@ActiveProfiles("test")
public class GreetingEndpointWebMvcIntegrationTests {

    private static final String MARK = "Mark";

	@Value("${local.server.port}") // injected by test framework = actual port used
	int port;

    @Autowired
    private GreetingService greetingService;

	@Before
	public void setup() {
		RestAssured.port = port;
	}

	@Test
	public void greetEndpointReturnsJsonOutputOfGreetingService() throws Exception {
		when().get(createEndpoint(MARK)).then().statusCode(HttpStatus.SC_OK).and().assertThat().body("message",
				equalTo(greetingService.createGreeting(MARK)));
	}

	@Test
	public void greetNameMissingReturnsNotFoundHttpResponse() throws Exception {
		when().get(createEndpoint("")).then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

    private String createEndpoint(final String name) {
    	return String.format("/greet/%s", name);
    }

}

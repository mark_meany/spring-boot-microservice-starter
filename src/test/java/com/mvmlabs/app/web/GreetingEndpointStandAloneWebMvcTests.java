package com.mvmlabs.app.web;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.mvmlabs.app.service.GreetingService;

/**
 * Test controller in MVC environment using Spring test framework.
 * 
 * Note, not using the SpringJUnit4ClassRunner - this is important!
 * 
 * @author mark
 */
public class GreetingEndpointStandAloneWebMvcTests {

    private MockMvc mockMvc;

    @Mock
    private GreetingService greetingService;
    
    @InjectMocks
    private GreetingEndpoint sut;
    
    @Before
    public void setup() {
    	MockitoAnnotations.initMocks(this);
    	
    	this.mockMvc = MockMvcBuilders.standaloneSetup(sut).build();
    }

    @Test
    public void greetPerson() throws Exception {
    	when(greetingService.createGreeting("Mark")).thenReturn("Hello Mark!");
        this.mockMvc.perform(get("/greet/Mark").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.message").value("Hello Mark!"));
    }

    @Test
    public void greetNameMissingReturnsNotFoundHttpResponse() throws Exception {
        this.mockMvc.perform(get("/greet/").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isNotFound());
    }

}

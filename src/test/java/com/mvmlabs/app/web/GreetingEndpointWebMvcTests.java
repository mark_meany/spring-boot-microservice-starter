package com.mvmlabs.app.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mvmlabs.app.SpringBootTestingApplication;
import com.mvmlabs.app.service.GreetingService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test controller in MVC environment using Spring test framework.
 * 
 * @author mark
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = SpringBootTestingApplication.class)
@ActiveProfiles("test")
public class GreetingEndpointWebMvcTests {

    private static final String MARK = "Mark";

	@Autowired
    private WebApplicationContext wac;

    @Autowired
    private GreetingService greetingService;
    
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void greetPerson() throws Exception {
        this.mockMvc.perform(get(createEndpoint(MARK)).accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.message").value(greetingService.createGreeting(MARK)));
    }

    @Test
    public void greetNameMissingReturnsNotFoundHttpResponse() throws Exception {
        this.mockMvc.perform(get(createEndpoint("")).accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isNotFound());
    }

    private String createEndpoint(final String name) {
    	return String.format("/greet/%s", name);
    }
    
}

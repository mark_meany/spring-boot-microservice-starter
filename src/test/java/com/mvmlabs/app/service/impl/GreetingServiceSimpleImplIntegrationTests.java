package com.mvmlabs.app.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mvmlabs.app.SpringBootTestingApplication;
import com.mvmlabs.app.service.GreetingService;

import lombok.val;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootTestingApplication.class)
@ActiveProfiles("test")
public class GreetingServiceSimpleImplIntegrationTests {

    private static final String MARK = "MyVeryLongName";

    @Autowired
    private GreetingService greetingService;

    @Value("${app.default.greeting.name}")
    private String expectedDefaultName;
    
	@Test
	public void testWhenNoNameProvidedUsesDefaultsNameInGreeting() {
		val greeting = greetingService.createGreeting(null);
		assertThat(greeting, containsString(expectedDefaultName));
	}

	@Test
	public void testNameProvidedItAppearsInTheGreeting() {
		val greeting = greetingService.createGreeting(MARK);
		assertThat(greeting, containsString(MARK));
	}


}

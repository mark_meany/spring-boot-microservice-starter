package com.mvmlabs.app.service.impl;

import lombok.val;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

public class GreetingServiceSimpleImplUnitTests {


	private static final String DEFAULT_GREETING_TEMPLATE = "Hello %s!";
	private static final String GREETING_TEMPLATE = "%s is an idiot!";
	private static final String DEFAULT_NAME = "John";
	private static final String JOHN = "John";
	private static final String MARK = "Mark";
	
	private GreetingServiceSimpleImpl sut = new GreetingServiceSimpleImpl();
	private final String expectedGreetingForDefaultName = String.format(DEFAULT_GREETING_TEMPLATE, DEFAULT_NAME);
	private final String expectedGreetingForMark = String.format(DEFAULT_GREETING_TEMPLATE, MARK);

	@Before
	public void setGreetingServiceToAPredictableImplementation() {
		ReflectionTestUtils.setField(sut, "defaultName", DEFAULT_NAME);
		ReflectionTestUtils.setField(sut, "greetingTemplate", DEFAULT_GREETING_TEMPLATE);
    }
	
	@Test
	public void testWhenNoNameProvidedUsesDefaultsNameInGreeting() {
		val greeting = sut.createGreeting(null);
		assertEquals("When no name provided expected default to be used.", expectedGreetingForDefaultName, greeting);
	}

	@Test
	public void testDefaultsNameCanBeChanged() {
		ReflectionTestUtils.setField(sut, "defaultName", JOHN);
		val greeting = sut.createGreeting(null);
		assertEquals("When no name provided expected default to be used.", String.format(sut.getGreetingTemplate(), JOHN), greeting);
	}

	@Test
	public void testGreetingTemplateCanBeChanged() {
		ReflectionTestUtils.setField(sut, "greetingTemplate", GREETING_TEMPLATE);
		val greeting = sut.createGreeting(null);
		assertEquals("Expected new template to be used for greeting.", String.format(GREETING_TEMPLATE, sut.getDefaultName()), greeting);
	}

	@Test
	public void testWhenEmptyNameProvidedUsesDefaultsNameInGreeting() {
		val greeting = sut.createGreeting("");
		assertEquals("When name provided is empty string expected default to be used.", expectedGreetingForDefaultName, greeting);
	}

	@Test
	public void testWhenNameContainingOnlyWhitespaceProvidedGreetsTheWorld() {
		val greeting = sut.createGreeting(" \t\n ");
		assertEquals("When name provided contains only whitespace expected default to be used.", expectedGreetingForDefaultName, greeting);
	}

	@Test
	public void testWhenNameProvidedGreetsThatName() {
		val greeting = sut.createGreeting("Mark");
		assertEquals("When no name provided expected 'World' to be used.", expectedGreetingForMark, greeting);
	}

	@Test
	public void testWhenNameProvidedIsSurroundedByWhitespaceTheWhitespaceIsRemovedAndGreetsJustTheName() {
		val greeting = sut.createGreeting("\tMark \n");
		assertEquals("When name provided is surrounded by whitespace expected whitespace to be stripped.", expectedGreetingForMark, greeting);
	}

}
